//
//  JSWViewController.h
//  NotesApp
//
//  Created by Mac Attack on 6/11/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSWNote.h"
#import "FMDatabase.h"

@interface JSWViewController : UIViewController {
    IBOutlet UITextField *noteTitle;
    IBOutlet UITextView *noteText;
    IBOutlet UITableView *notesTableView;
    NSMutableArray *notesArray;
}

@property (nonatomic, strong) IBOutlet UITableView *notesTableView;
@property (nonatomic, retain) IBOutlet UITextView *noteText;
@property (nonatomic, retain) IBOutlet UITextField *noteTitle;
@property (nonatomic, retain) NSString *noteTextString;
@property (nonatomic, retain) NSString *noteTitleString;
@property (nonatomic, readwrite) NSInteger noteID;

- (IBAction)saveNote:(id)sender;

@end
