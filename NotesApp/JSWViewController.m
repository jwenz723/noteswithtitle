//
//  JSWViewController.m
//  NotesApp
//
//  Created by Mac Attack on 6/11/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import "JSWViewController.h"

@interface JSWViewController ()

@end

@implementation JSWViewController
@synthesize notesTableView, noteText, noteTextString, noteTitle, noteTitleString, noteID;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Allocate an array to store all our notes
    notesArray = [[NSMutableArray alloc] init];
    
    // Apply the variable to the textfield in the view
    noteText.text = noteTextString;
    noteTitle.text = noteTitleString;

    // Load all the notes in the database into the table view
    [self updateList];
}

- (void)viewWillAppear:(BOOL)animated {
    // Load all the notes in the database into the table view
    [self updateList];
}

// Get the path where we will have our database
- (NSString *)dataFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [paths objectAtIndex:0];
    return docDirectory;
}

- (void)updateList {
    // remove all objects before we re-add everything
    [notesArray removeAllObjects];
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[self dataFilePath]stringByAppendingPathComponent:@"todo.sqlite"]];
    
    // This will create the database even if it doesn't already exist
    [database open];
    
    // create table
    [database executeUpdate:@"CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY, title VARCHAR(50), text TEXT)"];
    
    FMResultSet *results = [database executeQuery:@"SELECT * FROM notes"];
    
    // loop the the results
    while ([results next]) {
        JSWNote *temp = [[JSWNote alloc]init];
        temp.ID = [results intForColumn:@"id"];
        temp.title = [results stringForColumn:@"title"];
        temp.text = [results stringForColumn:@"text"];
        
        // Add the item into our list
        [notesArray addObject:temp];
    }
    [database close];
    
    // Update the view
    [notesTableView reloadData];
}

// Sets the amount of rows to be built in the tableview
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [notesArray count];
}

// This function creates the cells for the tableview
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *tableCellID = @"TableCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableCellID];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:tableCellID];
    }
    
    JSWNote* temp = (JSWNote *)[notesArray objectAtIndex:indexPath.row];

    // set the text to display in the table cell
    cell.textLabel.text = temp.title;
    
    return cell;
}

// Called every time the user touches a table cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    JSWNote *temp = (JSWNote *)[notesArray objectAtIndex:indexPath.row];
    
    JSWViewController *vc = (JSWViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"JSWNoteViewController"];
    
    // store the id, title, and text of the note that was just clicked into the new view controller
    vc.noteID = temp.ID;
    vc.noteTitleString = temp.title;
    vc.noteTextString = temp.text;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    FMDatabase *database = [FMDatabase databaseWithPath:[[self dataFilePath]stringByAppendingPathComponent:@"todo.sqlite"]];
    [database open];
    
    // delete the object from the database
    [database executeUpdate:@"DELETE FROM notes WHERE id = (?)", [NSNumber numberWithInt:[[notesArray objectAtIndex:indexPath.row] ID]]];
    [database close];
    
    [self updateList];
}

// When the user clicks the save button on the note viewer
- (IBAction)saveNote:(id)sender {
    FMDatabase *database = [FMDatabase databaseWithPath:[[self dataFilePath]stringByAppendingPathComponent:@"todo.sqlite"]];
    
    // This will create the database even if it doesn't already exist
    [database open];
    
    // If the note was created
    if (noteID == 0) {
        // insert the new todoItem into the database
        [database executeUpdate:@"INSERT INTO notes (title, text) VALUES (?, ?)", noteTitle.text, noteText.text];
    }
    // If the note was editted
    else {
        [database executeUpdate:@"UPDATE notes SET title = (?), text = (?) WHERE id = (?)", noteTitle.text, noteText.text, [NSNumber numberWithInt:noteID]];
    }
    
    [database close];
    
    // Display the root view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
