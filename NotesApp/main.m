//
//  main.m
//  NotesApp
//
//  Created by Mac Attack on 6/11/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JSWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JSWAppDelegate class]));
    }
}
