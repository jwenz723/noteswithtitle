//
//  JSWNote.h
//  NotesApp
//
//  Created by Mac Attack on 6/11/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSWNote : NSObject {
    NSInteger ID;
    NSString *title;
    NSString *text;
}

@property (nonatomic, readwrite) NSInteger ID;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *text;

@end
