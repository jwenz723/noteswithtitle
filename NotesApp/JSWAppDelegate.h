//
//  JSWAppDelegate.h
//  NotesApp
//
//  Created by Mac Attack on 6/11/14.
//  Copyright (c) 2014 Jeff Wenzbauer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
